<?php

require_once __DIR__ . '/vendor/autoload.php';

use jjpmann\ScreenGrab\MyRemoteWebDriver;
use jjpmann\ScreenGrab\SimpleScreenGrab as Grab;

    
    $file = false;
    $driver = MyRemoteWebDriver::start();
        
    //  Test

    try {
        $file = (new Grab($driver))->setUrl('http://www.lmo.com')->process()->save('./test.jpg', 5);
    } 
    catch (\Exception $e)  {
        $driver->close();
        throw new Exception($e, 1);
    }

    $driver->close();

    echo "<pre>"; var_dump( $file ); exit;
    