<?php namespace Newday;


class Zip {
	
	static public function check(){

		if( ! class_exists('ZipArchive') )
		{
			$message = 'ZipArchive must be installed.';

			throw new \Exception($message, 1);
		}
		
	}

	public static function make($filename, $files)
	{

		self::check();
		
		$zip = new \ZipArchive();
		
		
		if ($zip->open($filename, \ZipArchive::CREATE)!==TRUE) {

			$message = "cannot open <$filename>\n";

			throw new \Exception($message, 1);

		}
		
		$zip->addFromString('README.txt', 'all contents generated on http://demo.jjpmann.com');

		foreach( $files as $file )
		{
			$zip->addFile($file, substr($file,strrpos($file,'/') + 1));				
		}

		$zip->close();
		
	}

}