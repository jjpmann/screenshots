<?php namespace jjpmann\ScreenGrab;

use jjpmann\ScreenGrab\MyRemoteWebDriver as WebDriver;

abstract class ScreenGrab implements ScreenGrabInterface {
    
    protected $url;

    protected $webdriver;

    protected $screenshot;

    static protected $started = false;

    public function __construct(WebDriver $webdriver)
    {
        $this->webdriver = $webdriver;
    }

    public function get()
    {
        $this->webdriver->get($this->url);
        return $this;
    }

    public function save($file, $wait = 0)
    {       
        echo time()."\n";
        if ($wait) {
            sleep($wait);
        }
        $this->webdriver->takeScreenshot($file);
        echo time()."\n";    
        $this->screenshot = $file;
        return $file;
        
    }

    public function getScreenshot()
    {
        return $this->screenshot;
    }

    public function process($options = [])
    {
        if( ! self::$started )
        {
            $this->get();
            self::$started = true;
        }
        return $this;
    }


}