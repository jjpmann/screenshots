<?php namespace jjpmann\ScreenGrab;


interface ScreenGrabInterface {

	public function get();

	public function process($info);

	public function save($file);
	
}

