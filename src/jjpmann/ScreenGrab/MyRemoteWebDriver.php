<?php namespace jjpmann\ScreenGrab;


class MyRemoteWebDriver extends \RemoteWebDriver {

	const HOST = 'http://localhost:4444/wd/hub'; // this is the default

	static $driver;

	public static function check()
	{
		try {
			$capabilities = array(\WebDriverCapabilityType::BROWSER_NAME => 'firefox');
			self::$driver = self::create( self::HOST, $capabilities);	
		} catch (\WebDriverCurlException $e) {
			throw new MyRemoteWebDriverException('Selenium NOT Running!');
		}
		
	}

	
	public static function start()
	{

		$capabilities = array(\WebDriverCapabilityType::BROWSER_NAME => 'firefox');
		
		if( !self::$driver ){
			self::$driver = self::create( self::HOST, $capabilities);
		}

		// set windows size
		self::$driver->manage()->window()->setSize( new \WebDriverDimension(1090,1500) );
		
		return self::$driver;

	}

	public function takeScreenshot($save_as = null, $type='png') 
	{
		$screenshot = base64_decode(
			$this->executor->execute('takeScreenshot')
		);

	
		if ($save_as) {

			// convert to jpg??
			if( $type == 'jpg' || strpos( strrev($save_as), 'gpj.' ) === 0 || strpos( strrev($save_as), 'gepj.' ) === 0 ){
				$screenshot = $this->png2jpg($screenshot);
			}

			file_put_contents($save_as, $screenshot);
		}
		return $screenshot;
	}

	private function png2jpg($screenshot, $quality = 80) 
	{

		$image = imagecreatefromstring($screenshot);
		
		ob_start();
		imagejpeg($image, null, $quality);
		
		$jpg = ob_get_contents();

		ob_end_clean();

		imagedestroy($image);

		return $jpg;
	}

}